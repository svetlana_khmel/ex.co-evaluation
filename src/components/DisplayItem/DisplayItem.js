import React, { Component } from 'react';

import './DisplayItem.css'

import { VideoSourceTypes } from '../../types/VideoSourceTypes'

class displayItem extends Component {


    videoSourceConfig = VideoSourceTypes.find(source => {
        return this.props.itemData.source && source.name === this.props.itemData.source;
    });

    isValidItem = (type, source) => {
      return type && source ? true : false;
    };

    dateStrOption = { year: 'numeric', month: 'short', day: 'numeric' };

    numberToReadableText(num) {
        const number = Math.abs(Number(num));
        return number >= 1.0e+9 ?
            number / 1.0e+9 + "B" : number >= 1.0e+6 ?
                Math.round( (number / 1.0e+6) * 10 ) / 10 + "M" : number >= 1.0e+3 ?
                    number / 1.0e+3 + "K" : number;
    };

    secondsToTimeText(num) {
        let minutes = Math.floor(num / 60);
        let seconds = num - minutes * 60;

        return `${minutes}:${seconds}m`;
  };

    render() {
      const {itemData} = this.props;

      if(this.isValidItem(itemData.type, itemData.source)) {
        const dateStr = new Date(itemData.date).toLocaleDateString("en-US", this.dateStrOption);
        const readableViewsNumber = this.numberToReadableText(itemData.views);

        return (
          <div className="display-item">
            {itemData.url
              ? this.videoSourceConfig.getPlayer(itemData[this.videoSourceConfig.id])
              : <div className={'videoThumbnail'}></div>}
            <div className="display-item__details">
              <h2>{itemData.title}</h2>
              <div className="display-item__info">
                <p>{`${dateStr} - ${readableViewsNumber}`}</p>
              </div>
              <div className="display-item__bottom-bar">
              </div>
            </div>
          </div>
        );
      }
      return (
        <div className="display-item error-item">
          <div>ITEM NOT AVAILABLE</div>
        </div>
      );
    }
}

export default displayItem;